package com.example.RssF1news;

/**
 * Created by Denis on 29.11.2014.
 */

import android.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLPullParserHandler {
    List<News> newsList;
    private News news;
    private String text;

    public XMLPullParserHandler() {
        newsList = new ArrayList<News>();
    }

    public List<News> getEmployees() {
        return newsList;
    }

    public List<News> parse(InputStream is) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(false);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(is,null);
            boolean insideItem = false;
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    if (xpp.getName().equalsIgnoreCase("item")) {
                        insideItem = true;
                        news = new News();
                    } else if (xpp.getName().equalsIgnoreCase("title")) {
                        if (insideItem)
                            news.setTitle(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("link")) {
                        if (insideItem)
                            news.setLink(xpp.nextText());
                    }
                    else if (xpp.getName().equalsIgnoreCase("description")) {
                        if (insideItem){
                            String sourse = xpp.nextText();
                            String regex = "\"([^\"]*)\"";
                            Pattern pat = Pattern.compile(regex);
                            Matcher m = pat.matcher(sourse);
                            if(m.find()) {
                                news.setImageSourse(m.group(1));
                            }

                        }
                    }else if (xpp.getName().equalsIgnoreCase("pubDate")) {
                        if (insideItem)
                            news.setDate(xpp.nextText());
                    }

                } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                    insideItem = false;
                    newsList.add(news);
                }

                eventType = xpp.next();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return newsList;
    }
}
