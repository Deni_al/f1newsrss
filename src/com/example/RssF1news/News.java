package com.example.RssF1news;

/**
 * Created by Denis on 29.11.2014.
 */
public class News {
    public String title;
    public String description;
    public String date;
    public String link;
    public String imageSourse;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImageSourse() {
        return imageSourse;
    }

    public void setImageSourse(String imageSourse) {
        this.imageSourse = imageSourse;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", date='" + date + '\'' +
                ", link='" + link + '\'' +
                ", imageSourse='" + imageSourse + '\'' +
                '}';
    }
}
