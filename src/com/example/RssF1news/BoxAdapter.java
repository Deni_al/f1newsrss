package com.example.RssF1news;

/**
 * Created by Denis on 29.11.2014.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class BoxAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    ArrayList<News> objects;

    BoxAdapter(Context context, ArrayList<News> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = lInflater.inflate(R.layout.row, parent, false);
        }
        News p = getNews(position);
        DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);



        ((TextView) view.findViewById(R.id.title)).setText(p.getTitle());
        try {
             Date date = formatter.parse(p.getDate());
            formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            ((TextView) view.findViewById(R.id.date)).setText(formatter.format(date));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso.with(ctx).load(p.getImageSourse()).into((ImageView) view.findViewById(R.id.image));


        return view;
    }


    // получить элемент по позиции
    News getNews(int position) {
        return ((News) getItem(position));
    }





}
