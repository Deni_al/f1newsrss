package com.example.RssF1news;

/**
 * Created by Denis on 29.11.2014.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    final String RSS_SOURSE = "http://www.f1news.ru/export/news.xml";
    ListView listView;
    List<News> news = null;
    ProgressBar progressBar;
    TextView textView;
    static private final String CHOOSER_TEXT = "Load F1news Web Page with:";
    String url;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        listView = (ListView) findViewById(R.id.list);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        textView = (TextView) findViewById(R.id.textView);
        new FileReadTask().execute();
    }




    private class FileReadTask extends AsyncTask<Void, Void, Void> {
        String textResult;

        @Override
        protected Void doInBackground(Void... params) {
            if (isOnline()){
            URL textUrl;
            try {
                textUrl = new URL(RSS_SOURSE);
                BufferedReader bufferReader = new BufferedReader(new InputStreamReader(textUrl.openStream(), "Cp1251"));
                String StringBuffer;
                String stringText = "";
                while ((StringBuffer = bufferReader.readLine()) != null) {
                    stringText += StringBuffer;
                }
                bufferReader.close();
                textResult = stringText;
                writeToFile(textResult);
            }
            catch (MalformedURLException e) {
                textResult = e.toString();
            }
            catch (IOException e) {
                e.printStackTrace(); textResult = e.toString();
            }
            }
            else textResult = readFromFile();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            progressBar.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
            try {
                XMLPullParserHandler parser = new XMLPullParserHandler();
                news = parser.parse(new ByteArrayInputStream(textResult.getBytes("Cp1251")));
                BoxAdapter adapter =
                        new BoxAdapter(MyActivity.this, (ArrayList<News>) news);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> listView, View itemView, int itemPosition, long itemId)
                    {
                        url = news.get(itemPosition).getLink();
                        AlertDialog.Builder alert = new AlertDialog.Builder(MyActivity.this);
                        alert.setTitle("Open full news on website");
                        alert.setMessage("Click below to see more!");

                        alert.setIcon(null);
                        alert.setPositiveButton("Open news", myClickListener);
                        alert.setNeutralButton("Not Now", myClickListener);

                        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialog) {
                                Log.d("OK!", "Cancel");
                            }
                        });
                        alert.show();

                    }
                });



            } catch (IOException e) {
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Log.d("OK!", "BUTTON_POSITIVE clicked.");
                    Uri webpage = Uri.parse(url);

                    Intent baseIntent = new Intent(Intent.ACTION_VIEW, webpage);

                    Intent chooserIntent = Intent.createChooser(baseIntent, CHOOSER_TEXT);
                    if (baseIntent.resolveActivity(getPackageManager()) != null) {
                        startActivity(chooserIntent);
                    }
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.d("OK!", "BUTTON_NEGATIVE clicked.");
                    break;
            }
        }
    };


    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput("config.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


}
